
<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en"  direction="rtl" style="direction: rtl;" >
    <!-- begin::Head -->
    <head>
        <meta charset="utf-8" />

        <title>{{ __('Login') }}</title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

        <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
        <!--end::Web font -->

        <!--begin::Global Theme Styles -->
                    <link href="/test/css/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />
                    <link href="/test/css/style.bundle.rtl.css" rel="stylesheet" type="text/css" />
                    {{-- <link href="/test/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> --}}
                    <link href="/test/css/style.css" rel="stylesheet" type="text/css" />
                <!--end::Global Theme Styles -->




        <link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />
    </head>
    <!-- end::Head -->


    <!-- begin::Body -->
    <body  class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >



    	<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">


				<div class="m-grid__item m-grid__item--fluid m-grid m-grid--desktop m-grid--ver-desktop m-grid--hor-tablet-and-mobile m-login m-login--6" id="m_login">
	<div class="m-grid__item   m-grid__item--order-tablet-and-mobile-2  m-grid m-grid--hor m-login__aside " style="background-image: url(test/img/bg-4.jpg);">
		<div class="m-grid__item">
			<div class="m-login__logo">
				<a href="#">
					<img src="test/img/logo-4.png">
				</a>
			</div>
		</div>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver">

			<div class="m-grid__item m-grid__item--middle">
				<span class="m-login__title">ورود به حساب کاربری</span>
				{{-- <span class="m-login__subtitle">Lorem ipsum amet estudiat</span> --}}
			</div>
		</div>

		<div class="m-grid__item">
			<div class="m-login__info">
				<div class="m-login__section">
					<a href="#" class="m-link">&copy 2018 Metronic</a>
				</div>
				<div class="m-login__section">
					<a href="#" class="m-link">{{ __('messages.Privacy') }}</a>
					<a href="#" class="m-link">{{ __('messages.Legal') }}</a>
					<a href="#" class="m-link">{{ __('messages.Contact') }} </a>
				</div>
			</div>
		</div>
	</div>

	<div class="m-grid__item m-grid__item--fluid  m-grid__item--order-tablet-and-mobile-1  m-login__wrapper">
		<!--begin::Head-->
		<div class="m-login__head">
      @guest
			<span>{{ __('messages.dont') }}</span>
			<a href="{{ route('register') }}" class="m-link m--font-danger">{{ __('messages.register') }}</a>
      @else
      <a href="{{ route('logout') }}" class="m-link m--font-danger">{{ __('messages.Logout') }}</a>
      @endguest
		</div>
		<!--end::Head-->

		<!--begin::Body-->
		<div class="m-login__body">

			<!--begin::Signin-->
			<div class="m-login__signin">
				<div class="m-login__title">
					<h3>{{ __('messages.login') }}</h3>
				</div>

				<!--begin::Form-->
			<form class="m-login__form m-form" method="POST" action="{{ route('login') }}">
              @csrf
					<div class="form-group m-form__group">
						<!-- <input class="form-control m-input" type="text" placeholder="Username" name="username" autocomplete="off"> -->
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} m-input" name="email" value="{{ old('email') }}" required autofocus>


            @if ($errors->has('email'))

                    <strong>{{ $errors->first('email') }}</strong>

            @endif
					</div>
					<div class="form-group m-form__group">

            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} m-input m-login__form-input--last" name="password" required>

            @if ($errors->has('password'))

                    <strong>{{ $errors->first('password') }}</strong>

            @endif
					</div>
          <div class="form-group">

                  <div class="form-check" style="padding:30px;text-align:right">
                      <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                      <label class="form-check-label" for="remember" >
                          {{ __('messages.Remember') }}

                      </label>
                  </div>

          </div>
          <!--begin::Action-->
          <div class="m-login__action">
            <a href="#" class="m-link">
              <span> {{ __('messages.forget') }}</span>
            </a>
            <a href="#">
              <button type="submit" id="m_login_signin_submit" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">{{ __('messages.login') }}</button>
            </a>
          </div>

          <!--end::Action-->
			</form>
				<!--end::Form-->



				<!--begin::Divider-->
				<div class="m-login__form-divider">
					<div class="m-divider">
						<span></span>
						<span>OR</span>
						<span></span>
					</div>
				</div>
				<!--end::Divider-->

				<!--begin::Options-->
				<div class="m-login__options">
					<a href="#" class="btn btn-primary m-btn m-btn--pill  m-btn  m-btn m-btn--icon">
						<span>
							{{-- <i class="fa fa-facebook-f"></i> --}}
							<span>{{ __('messages.Facebook') }}</span>
						</span>
					</a>

					<a href="#" class="btn btn-info m-btn m-btn--pill  m-btn  m-btn m-btn--icon">
						<span>
							{{-- <i class="fa fa-twitter"></i> --}}
							<span>{{ __('messages.Twitter') }}</span>
						</span>
					</a>

					<a href="#" class="btn btn-danger m-btn m-btn--pill  m-btn  m-btn m-btn--icon">
						<span>
							{{-- <i class="fa fa-google"></i> --}}
							<span>{{ __('messages.Google') }}</span>
						</span>
					</a>
				</div>
				<!--end::Options-->
			</div>
			<!--end::Signin-->
		</div>
		<!--end::Body-->
	</div>
</div>




</div>
<!-- end:: Page -->


        <!--begin::Global Theme Bundle -->

                <!--end::Global Theme Bundle -->


                    <!--begin::Page Scripts -->
                            {{-- <script src="test/js/login6.js" type="text/javascript"></script> --}}
                        <!--end::Page Scripts -->

                    </body>
    <!-- end::Body -->
</html>
