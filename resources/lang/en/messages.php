<?php

return [
    'register' => 'register',
    'login' => 'login',
    'logout' => 'logout',
    'dont' => 'dont have any account?',
    'google' => 'google',
    'Remember' => 'remember me',
    'Facebook' => 'Facebook',
    'Twitter' => 'Twitter',
    'Google' => 'Google',
    'Contact' => 'Contact',
    'Legal' => 'Legal',
    'Privacy' => 'Privacy',
    'forget' => 'Forgot Your Password?
',
];
