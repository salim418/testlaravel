<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'اطلاعات وارد شده صحیح نیست.',
    'throttle' => 'دفعات ورود اطلاعات اشتباه بیشتر از حد مجاز است. لطفا بعدا مجددا تلاش نمایید.',

];
