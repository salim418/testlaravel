<?php

return [
    'register' => 'ثبت نام',
    'login' => 'ورود',
    'logout' => 'خروج',
    'loginaccount' => 'ورود به حساب کاربری',
    'google' => 'گوگل',
    'dont' => 'حساب کاربری ندارید؟',
    'Remember' => 'مرا به خاطر بسپار',
    'Facebook' => 'فیس بوک',
    'Twitter' => 'توییتر',
    'Google' => 'گوگل',
    'Contact' => 'تماس',
    'Legal' => 'قوانین',
'Privacy' => 'حریم خصوصی',
    'forget' => 'رمز عبور خود را فراموش کرده اید؟'

];
