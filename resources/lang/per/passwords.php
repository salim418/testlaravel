<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'رمز عبور باید حداقل 6 کاراکتر باشد و با تکرار آن برابر باشد',
    'reset' => 'رمز عبور شما با موفقیت رست شد!',
    'sent' => 'یک لینک جهت تغییر رمز عبور برای شما ایمیل شد!',
    'token' => 'توکن رمز عبور صحصیح نیست.',
    'user' => "کاربری با این مشخصات یافت نشد!",

];
